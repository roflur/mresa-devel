
n <- 12
m <- 9

control_grid <- expand.grid(1:n, 1:m)
control_dists <- as.matrix(dist(control_grid))

A <- spam::precmat.IGMRFreglat(n,m)
loc1_grid <- mresa::prec2grid(1:108, m, n)
loc2_grid <- mresa::prec2grid(1:108, m, n)

distances <- sapply(1:dim(loc1_grid)[1], function(x) { sapply(1:dim(loc1_grid)[1], function(y){
  mresa::euclidean_distance(p = loc1_grid[x, ], q = loc2_grid[y, ]) }) })

table(distances) == table(control_dists)
control_dists == t(distances)


Sigma <- spam::as.spam(solve(as.matrix(A + diag(1, n*m, n*m))))
dim1 <- Sigma@dimension[1]
dim2 <- Sigma@dimension[2]

# options(spam.trivalues = TRUE)
# lowtriSigma <- spam::lower.tri.spam(spam::as.spam(Sigma), diag = TRUE)
#
# if (length(lowtriSigma@entries) <= 1) {
#   stop("spam.eps is probably not small enough, as.spam introduces a zero matrix") }
#
# distances <- numeric(length = length(lowtriSigma@entries))
# loc1 <- loc2 <- result <- matrix(NA, nrow = length(lowtriSigma@entries), ncol = 2)
# tripletlowtriSigma <- spam::triplet(lowtriSigma, tri = TRUE)
#
# loc1 <- mresa::prec2grid(tripletlowtriSigma$i, n = n, m = m)
# loc2 <- mresa::prec2grid(tripletlowtriSigma$j, n = n, m = m)
#
# distances <- sapply(1:dim(loc1)[1], function(x) {
#   mresa::euclidean_distance(p = loc1[x, ], q = loc2[x, ]) })
#
# result <- cbind(cov = lowtriSigma@entries, dists = distances)
#
#
# table(result)

# lowtriSigma <- spam::lower.tri.spam(spam::as.spam(Sigma), diag = TRUE)
# if (length(lowtriSigma@entries) <= 1) {
#   stop("spam.eps is probably not small enough, as.spam introduces a zero matrix") }
# distances <- numeric(length = length(lowtriSigma@entries))
# loc1 <- loc2 <- result <- matrix(NA, nrow = length(lowtriSigma@entries), ncol = 2)
#
# if (cov) {
#
#   tripletlowtriSigma <- spam::triplet(lowtriSigma)
#   loc1 <- prec2grid(tripletlowtriSigma$indices[,1], n = n, m = m)
#   loc2 <- prec2grid(tripletlowtriSigma$indices[,2], n = n, m = m)
#
#   distances <- sapply(1:dim(loc1)[1], function(x) {
#     euclidean_distance(p = loc1[x, ], q = loc2[x, ]) })
#
#   result <- cbind(corrs = lowtriSigma@entries, dists = distances)
#
#   options(spam.eps = prespameps)
#   options(spam.trivalues = prespamtrivalues)
#   return(result)
# } else {
  # vars <- spam::diag.spam(lowtriSigma)
  # tripletlowtriSigma <- spam::triplet(lowtriSigma, tri = TRUE)
  # corrvalues <- sapply(1:length(tripletlowtriSigma$values), function(x) {
  #   result <- tripletlowtriSigma$values[x]/sqrt(vars[tripletlowtriSigma$i[x]] * vars[tripletlowtriSigma$j[x]]) })

# }




















