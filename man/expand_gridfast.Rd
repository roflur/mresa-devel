% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{expand_gridfast}
\alias{expand_gridfast}
\title{faster expand.grid}
\usage{
expand_gridfast(x, y)
}
\arguments{
\item{x}{sequence in x direction.}

\item{y}{sequence in y direction.}
}
\value{
a matrix of two columns, containing all possible combinations from \code{x} and \code{y}.
}
\description{
faster expand.grid
}
